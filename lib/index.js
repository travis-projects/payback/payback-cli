const inquirer = require('inquirer')

// Import Functions
const { auth } = require('../lib/auth')
const { project } = require('../lib/project')

// Start Options/Data collecting
const authSelect = async () => {
  const login = await inquirer.prompt([{
    type: 'list',
    name: 'login',
    message: 'Do you want to login or register?',
    choices: ['Login', 'Register']
  }]).then(({login}) => login)
  if (login === 'Login') {
    const loginDetails = await inquirer.prompt([
      {
        type: 'input',
        name: 'email',
        message: 'Enter your email address'
      },
      {
        type: 'password',
        name: 'password',
        message: 'Enter your password'
      }
    ]).then(answers => answers)
    const login = await auth.login(loginDetails)
    return console.log(login)
  } else {
    const signupDetails = await inquirer.prompt([
      {
        type: 'input',
        name: 'email',
        message: 'Enter your email address'
      },
      {
        type: 'password',
        name: 'password',
        message: 'Enter your password'
      }
    ]).then(answers => answers)
    const signup = await auth.signup(signupDetails)
    return console.log(signup)
  }
}

const init = async (projectName) => {
  if (projectName) {
    const createProject = await project.createProject(projectName)
    return console.log(createProject)
  } else {
    const projectName = await inquirer.prompt([{
      type: 'input',
      name: 'projectName',
      message: 'Project Name'
    }]).then(({projectName}) => projectName)
    const createProject = await project.createProject(projectName)
    return console.log(createProject)
  }
}

const update = async () => {
  const updateProject = await project.updateProject()
  return console.log(updateProject)
}

const delete = async () => {
  const deleteProject = await project.deleteProject()
  return deleteProject
}

module.exports = { authSelect, init, update, delete }
