### Version 0.0.1-4
- Changed back to rc package to stop not found error
- Changed .*rc creation method

### Version 0.0.1-3
- Edited to add local package deps, and dev deps

### Version 0.0.1-2
- Added loading spinners

### Version 0.0.1-1
- Added project creation
