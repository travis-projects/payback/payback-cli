#!/usr/bin/env node
const { authSelect, init, update, delete } = require('../lib/index')

const payback = async () => {
  const args = process.argv.splice(process.execArgv.length + 2)
  const method = args[0]
  if (!method) authSelect()
  else if (method === 'init') init(args[1])
  else if (method === 'update') update()
  else if (method === 'delete') delete()
  else console.log(`Don't recognise that command?`)
}

payback()
